# ReactMP

This is an example of a minimalistic ReactMP (Multi-platform) "Hello, world" style.

It is intended to serve as an experiment or for educational purposes. Do not use this in production!

What's included:

* Support for iOS, Android, and web
* TypeScript
* Functional-style
* npm instead of yarn
* CI for Azure Pipelines

What's **not** included:

* Test suite with Jest
* Expo and its related tooling
* Formatting with Prettier
* Static analysis with ESLint/TSLint
* CI for other systems
